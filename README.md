# ReCaptcha Headless

This module provides recaptcha validation for configured routes. 
It adds access requirement for the configured routes. 
The access check service validates response of recaptcha.

## Getting Started

  * You need to have a Google recaptcha site Key and secret key.
  * You need to configure routes for validation.
  * Recaptcha response should be set into X-Recaptcha header.
