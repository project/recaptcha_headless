<?php

namespace Drupal\recaptcha_headless\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\recaptcha_headless\Access\ReCaptchaHeadlessAccess;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The config for settings recaptcha.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('recaptcha_headless.settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($collection->all() as $route_name => $route) {
      if ($this->validation($route_name)) {
        $route->setRequirement(ReCaptchaHeadlessAccess::ROUTE_REQUIREMENT_KEY, 'TRUE');
      }
    }
  }

  /**
   * Validation route configurations.
   *
   * @param string $route_name
   *   Current Route object.
   *
   * @return bool
   *   Returns TRUE if route on config settings.
   */
  protected function validation(string $route_name) {
    foreach ($this->config->get('routes') as $name) {
      $exclusion = substr($name, 0, 1) === '~';
      if ($exclusion) {
        $name = substr($name, 1);
      }

      $regexable = preg_quote($name, '/');

      $regexable = str_replace('\*', '.*', $regexable);
      if (preg_match('/^' . $regexable . '$/', $route_name)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
