<?php

namespace Drupal\recaptcha_headless\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use ReCaptcha\ReCaptcha;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Class ReCaptchaHeadlessAccess.
 *
 * @package Drupal\recaptcha_headless\Access
 */
class ReCaptchaHeadlessAccess implements AccessInterface {

  /**
   * The route requirement key for this access check.
   *
   * @var string
   */
  const ROUTE_REQUIREMENT_KEY = '_recaptcha_headless_access';

  /**
   * The config for settings recaptcha.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * ReCaptchaAccess constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('recaptcha_headless.settings');
  }

  /**
   * Checks access to the relationship field on the given route.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming HTTP request object.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Request $request, Route $route, AccountInterface $account) :AccessResultInterface {
    $response = $request->headers->get('X-Recaptcha');
    $recaptcha_secret_key = $this->config->get('secret_key');
    $recaptcha = new ReCaptcha($recaptcha_secret_key);

    $verify = $recaptcha->verify($response);

    if (!$verify->isSuccess()) {
      $messages = self::errorMessagesByCodes($verify->getErrorCodes());
      return AccessResult::forbidden(
        !empty($messages) ? 'ReCaptcha: ' . implode(PHP_EOL, $messages) : ' Error in ReCaptcha.'
      );
    }

    return AccessResult::allowed();
  }

  /**
   * Get list of error messages.
   *
   * @param array $codes
   *   The array of reCAPTCHA service codes.
   *
   * @return array
   *   Return error messages.
   */
  protected static function errorMessagesByCodes(array $codes) {
    return array_filter(self::errorMessages(), function ($code) use ($codes) {
      return array_search($code, $codes) !== FALSE;
    }, ARRAY_FILTER_USE_KEY);
  }

  /**
   * The array of reCAPTCHA service codes.
   *
   * @return array
   *   Returns array of reCAPTCHA service codes.
   */
  protected static function errorMessages() {
    return [
      'action-mismatch' => t('Expected action did not match.'),
      'apk_package_name-mismatch' => t('Expected APK package name did not match.'),
      'bad-response' => t('Did not receive a 200 from the service.'),
      'bad-request' => t('The request is invalid or malformed.'),
      'challenge-timeout' => t('Challenge timeout.'),
      'connection-failed' => t('Could not connect to service.'),
      'invalid-input-response' => t('The response parameter is invalid or malformed.'),
      'invalid-input-secret' => t('The secret parameter is invalid or malformed.'),
      'invalid-json' => t('The json response is invalid or malformed.'),
      'missing-input-response' => t('The response parameter is missing.'),
      'missing-input-secret' => t('The secret parameter is missing.'),
      'hostname-mismatch' => t('Expected hostname did not match.'),
      'score-threshold-not-met' => t('Score threshold not met.'),
      'timeout-or-duplicate' => t('The challenge response timed out or was already verified.'),
      'unknown-error' => t('Not a success, but no error codes received!'),
    ];
  }

}
