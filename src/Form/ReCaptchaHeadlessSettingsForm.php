<?php

namespace Drupal\recaptcha_headless\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Class ReCaptchaHeadlessSettingsForm.
 */
class ReCaptchaHeadlessSettingsForm extends ConfigFormBase {

  /**
   * The router builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'recaptcha_headless.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recaptcha_headless_settings';
  }

  /**
   * ReCaptchaHeadlessSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $router_builder
   *   The router builder service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilderInterface $router_builder) {
    parent::__construct($config_factory);

    $this->routerBuilder = $router_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('recaptcha_headless.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];

    $form['general']['recaptcha_site_key'] = [
      '#default_value' => $config->get('site_key'),
      '#description' => $this->t('The site key given to you when you <a href=":url">register for reCAPTCHA</a>.', [':url' => 'https://www.google.com/recaptcha/admin']),
      '#maxlength' => 40,
      '#required' => TRUE,
      '#title' => $this->t('Site key'),
      '#type' => 'textfield',
    ];

    $form['general']['recaptcha_secret_key'] = [
      '#default_value' => $config->get('secret_key'),
      '#description' => $this->t('The secret key given to you when you <a href=":url">register for reCAPTCHA</a>.', [':url' => 'https://www.google.com/recaptcha/admin']),
      '#maxlength' => 40,
      '#required' => TRUE,
      '#title' => $this->t('Secret key'),
      '#type' => 'textfield',
    ];

    $form['general']['routes'] = [
      '#type' => 'textarea',
      '#title' => t('Routes to include'),
      '#description' => t('One configuration name per line.<br/>Examples: <ul><li>user.login</li><li>user.reset</li></ul>'),
      '#default_value' => implode(PHP_EOL, $config->get('routes') ?? []),
      '#rows' => 15,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $routes = array_map('trim',
      array_filter(explode(PHP_EOL, $values['routes']))
    );

    /* @var \Drupal\Core\Routing\RouteProviderInterface $route_provider */
    $route_provider = \Drupal::service('router.route_provider');

    foreach ($routes as $route) {
      try {
        $route_provider->getRouteByName($route);
      }
      catch (RouteNotFoundException $exception) {
        if (strpos($route, '*') === FALSE && strpos($route, '~') === FALSE) {
          $form_state->setErrorByName($route, t('Route %name does not exist', [
            '%name' => $route,
          ]));
        }
      }
    }

    $form_state->setValue('routes', $routes);

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('recaptcha_headless.settings');

    $config
      ->set('site_key', $form_state->getValue('recaptcha_site_key'))
      ->set('secret_key', $form_state->getValue('recaptcha_secret_key'))
      ->set('routes', $form_state->getValue('routes'))
      ->save();

    $this->routerBuilder->rebuild();

    parent::submitForm($form, $form_state);
  }

}
